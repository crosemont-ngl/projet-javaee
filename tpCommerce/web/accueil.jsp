<%-- 
    Document   : index
    Created on : 2020-11-22, 22:16:49
    Author     : Nadia Garcia
--%>

<%@page import="com.model.entities.Item"%>
<%@page import="com.model.entities.Animal"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.model.entities.Produit"%>
<%@page import="com.model.dao.ProduitDAO"%>
<%@page import="com.model.entities.Utilisateur"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>E-KUSA |  La boutique pour vous et votre famille</title>
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/font-awesome.min.css" rel="stylesheet">
        <link href="css/prettyPhoto.css" rel="stylesheet">
        <link href="css/animate.css" rel="stylesheet">
        <link href="css/main.css" rel="stylesheet">
        <link href="css/responsive.css" rel="stylesheet">       
        <link rel="shortcut icon" href="images/icono.png">
        <link rel="icon" href="images/icono.png" type="image/x-icon">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
        <style><%@include file="/css/alertMessage_style.css" %></style>
        <style><%@include file="/css/style_modal_accueil.css" %></style>
       
    </head><!--/head-->

    <body>

        
        <!--Header -->
        <jsp:include page="/WEB-INF/commons/header_accueil.jsp"/>
      
      

        <section id="slider"><!--slider-->
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div id="slider-carousel" class="carousel slide" data-ride="carousel">
                            <ol class="carousel-indicators">
                                <li data-target="#slider-carousel" data-slide-to="0" class="active"></li>
                                <li data-target="#slider-carousel" data-slide-to="1"></li>
                                <li data-target="#slider-carousel" data-slide-to="2"></li>
                            </ol>

                            <div class="carousel-inner">
                                <div class="item active">
                                    <div class="col-sm-6">
                                        <h1><span>E</span>-KUSA</h1>
                                        <h3>Boutique des Fêtes avec les meilleurs produits</h3>
                                        <p>Magasinez en cadeaux pour gâter votre fidèle compagnon </p>
                                        <button type="button" class="btn btn-default get">Nos produits</button>
                                    </div>
                                    <div class="col-sm-6">
                                        <img src="images/home/girl4.jpg" class="girl img-responsive" alt="" />

                                    </div>
                                </div>
                                <div class="item">
                                    <div class="col-sm-6">
                                        <h1><span>E</span>-KUSA</h1>
                                        <h2>10% de réduction sur les nourritures pour chatons</h2>
                                        <p>Visitez nos succursales et bénéficiez de remises spéciales.</p>
                                        <button type="button" class="btn btn-default get">Promotions</button>
                                        <!--a href="shop.html" class="btn btn-default get">Promotions</a-->
                                    </div>
                                    <div class="col-sm-6">
                                        <img src="images/home/girl5.jpg" class="girl img-responsive" alt="" />

                                    </div>
                                </div>

                                <div class="item">
                                    <div class="col-sm-6">
                                        <h1><span>E</span>-KUSA</h1>
                                        <h2>Achetez en ligne sans quitter la maison</h2>
                                        <p>Profitez des offres, des bas prix d'achats. </p>
                                        <button type="button" class="btn btn-default get">Club Patte Douce</button>
                                    </div>
                                    <div class="col-sm-6">
                                        <img src="images/home/girl7.jpg" class="girl img-responsive" alt="" />

                                    </div>
                                </div>

                            </div>

                            <a href="#slider-carousel" class="left control-carousel hidden-xs" data-slide="prev">
                                <i class="fa fa-angle-left"></i>
                            </a>
                            <a href="#slider-carousel" class="right control-carousel hidden-xs" data-slide="next">
                                <i class="fa fa-angle-right"></i>
                            </a>
                        </div>

                    </div>
                </div>
            </div>
        </section><!--/slider-->

        <hr/><!--confiance-->
        <p align="center">                                     
            <img alt="Éléments de confiance" src="images/home/confianza.png">
            <img alt="Éléments de confiance" src="images/home/confianza2.png">
            <img alt="Éléments de confiance" src="images/home/confianza3.png">
            <img alt="Éléments de confiance" src="images/home/confianza4.png">
        </p><!--/confiance-->
        <hr/>
        
        <section>
            <div class="container">
                <div class="row">
                    <div class="col-sm-3">
                        <div class="left-sidebar">
                            <h2>Catégories</h2>
                            <div class="panel-group category-products" id="accordian"><!--category-productsr-->
                                <c:forEach var="animal" items="${animals}" >  
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a href="${pageContext.request.contextPath}/Controlleur?action=listerProduitsSelectiones&idAnimal=${animal.idAnimal}">
                                                    <span class="badge pull-right"><i class="fa fa-plus"></i></span>
                                                        ${animal.nomAnimal}
                                                </a>
                                            </h4>
                                        </div>             
                                    </div>
                                </c:forEach>                                                       
                            </div><!--/category-products-->
                            <div class="shipping text-center"><!--shipping-->
                                <img src="images/home/shipping2.jpg" alt="" />
                            </div><!--/shipping-->
                        </div>
                    </div>

                    <div class="col-sm-9 padding-right">
                        <div class="features_items"><!--features_items-->
                            <h2 class="title text-center">Les plus demandés</h2>
                            <%if (session.getAttribute("produitsSelectiones") != null) {%>
                            <c:forEach var="produit" items="${produitsSelectiones}" >  
                                <div class="col-sm-4">
                                    <div class="product-image-wrapper">
                                        <div class="single-products">
                                            <div class="productinfo text-center">
                                                <img src="images/home/${produit.getImage()}" alt="" />
                                                <h2>${produit.getPrix()} $</h2>
                                                <p>${produit.getDescription()}</p>
                                                <span class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Ajouter au panier</span>
                                            </div>
                                            <div class="product-overlay">
                                                <div class="overlay-content">
                                                    <h2>${produit.getPrix()} $</h2>
                                                    <p>${produit.getDescription()}</p>
                                                    <a href="${pageContext.request.contextPath}/Panier?idProduit=${produit.idProduit}&action=order" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Ajouter au panier</a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="choose">
                                            <ul class="nav nav-pills nav-justified">
                                                <li><a href=""><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i></a></li>
                                                <li><a href="#"><i class="fa fa-check-circle"></i>Disponible</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </c:forEach> <%} else {%>

                            <c:forEach var="produit" items="${produits}" >  
                                <div class="col-sm-4">
                                    <div class="product-image-wrapper">
                                        <div class="single-products">
                                            <div class="productinfo text-center">
                                                <img src="images/home/${produit.getImage()}" alt="" />
                                                <h2>${produit.getPrix()} $</h2>
                                                <p>${produit.getDescription()}</p>
                                                <span class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Ajouter au panier</span>
                                            </div>
                                            <div class="product-overlay">
                                                <div class="overlay-content">
                                                    <h2>${produit.getPrix()} $</h2>
                                                    <p>${produit.getDescription()}</p>
                                                    <a href="${pageContext.request.contextPath}/Panier?idProduit=${produit.idProduit}&action=order" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Ajouter au panier</a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="choose">
                                            <ul class="nav nav-pills nav-justified">
                                                <li><a href=""><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i></a></li>
                                                <li><a href="#"><i class="fa fa-check-circle"></i>Disponible</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </c:forEach>

                            <%}%>
                            
                        </div><!--features_items-->

                        <div class="recommended_items"><!--recommended_items-->
                            <h2 class="title text-center">Articles suggérés</h2>

                            <div id="recommended-item-carousel" class="carousel slide" data-ride="carousel">
                                <div class="carousel-inner">

                                    <div class="item active">
                                        <c:forEach var="product" items="${productsActive}"> 
                                            <div class="col-sm-4">
                                                <div class="product-image-wrapper">
                                                    <div class="single-products">
                                                        <div class="productinfo text-center">
                                                            <img src="images/home/${product.getImage()}" alt="" />
                                                            <h2>${product.getPrix()}</h2>
                                                            <p>${product.getDescription()}</p>
                                                            <a href="${pageContext.request.contextPath}/Panier?idProduit=${product.idProduit}&action=order" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Ajouter au panier</a>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>                                     
                                        </c:forEach> 
                                    </div>

                                    <div class="item">	
                                        <c:forEach var="product" items="${productsItem}"> 
                                            <div class="col-sm-4">
                                                <div class="product-image-wrapper">
                                                    <div class="single-products">
                                                        <div class="productinfo text-center">
                                                            <img src="images/home/${product.getImage()}" alt="" />
                                                            <h2>${product.getPrix()}</h2>
                                                            <p>${product.getDescription()}</p>
                                                            <a href="${pageContext.request.contextPath}/Panier?idProduit=${product.idProduit}&action=order" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Ajouter au panier</a>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </c:forEach> 
                                    </div>
                                </div>
                                <a class="left recommended-item-control" href="#recommended-item-carousel" data-slide="prev">
                                    <i class="fa fa-angle-left"></i>
                                </a>
                                <a class="right recommended-item-control" href="#recommended-item-carousel" data-slide="next">
                                    <i class="fa fa-angle-right"></i>
                                </a>			
                            </div>
                        </div><!--/recommended_items-->

                    </div>
                </div>
            </div>
        </section>

      
        
        <!--Footer-->
        <jsp:include page="/WEB-INF/commons/footer.jsp"/>
         
        <!--Modal Login / Inscription-->
        <jsp:include page="/WEB-INF/forms/multi_modal.jsp"/>
        
        <!-- Validation password-->
        <script><jsp:include page="js/validation_password.js"/></script>



        <script src="js/jquery.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/jquery.scrollUp.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
        <script src="js/jquery.prettyPhoto.js"></script>
        <script src="js/main.js"></script>
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    </body>
</html>

