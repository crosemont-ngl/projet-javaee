<%-- 
    Document   : cart
    Created on : 2020-12-02, 22:16:49
    Author     : Nadia Garcia
--%>

<%@page import="com.model.entities.Animal"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.model.entities.Produit"%>
<%@page import="com.model.dao.ProduitDAO"%>
<%@page import="com.model.entities.Utilisateur"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>E-KUSA |  La boutique pour vous et votre famille</title>
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/font-awesome.min.css" rel="stylesheet">
        <link href="css/prettyPhoto.css" rel="stylesheet">
        <link href="css/animate.css" rel="stylesheet">
        <link href="css/main.css" rel="stylesheet">
        <link href="css/responsive.css" rel="stylesheet">       
        <link rel="shortcut icon" href="images/icono.png">
        <link rel="icon" href="images/icono.png" type="image/x-icon">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
        <style><%@include file="/css/alertMessage_style.css" %></style>
        <style><%@include file="/css/style_modal_accueil.css" %></style>

    </head><!--/head-->
    <body>
        <!--Header Client-->
        <jsp:include page="/WEB-INF/commons/header_client.jsp"/>

        <section id="cart_items">
            <div class="container">
                <div class="breadcrumbs">
                    <ol class="breadcrumb">
                        <li><a href="Panier">Panier</a></li>
                        <li class="active">Liste de produits</li>
                    </ol>
                </div>
                <div class="table-responsive cart_info">
                    <table class="table table-condensed">
                        <thead>
                            <tr class="cart_menu">
                                <td class="image">Produit</td>
                                <td class="description"></td>
                                <td class="price">Prix</td>
                                <td class="quantity">Quantité</td>
                                <td class="total">Total</td>
                                <td></td>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach items="${sessionScope.cart}" var="pro">
                                <c:set  var="total" value="${total+pro.p.prix*pro.quantite}"/>
                                <tr>
                                    <td class="cart_product">
                                        <a href=""><img src="images/home/${pro.p.image}" width="110px" alt=""></a>
                                    </td>
                                    <td class="cart_description">
                                        <h4><a href="">${pro.p.nomProduit}</a></h4>
                                        <p>ID Référence: ${pro.p.idProduit}</p>
                                    </td>
                                    <td class="cart_price">
                                        <p>${pro.p.prix}</p>
                                    </td>
                                    <td class="cart_quantity">
                                        <div class="cart_quantity_button">
                                            <a class="cart_quantity_up" href="${pageContext.request.contextPath}/Panier?idProduit=${pro.p.idProduit}&action=sumar"> + </a>								
                                            <input class="cart_quantity_input" type="text" name="quantity" value="${pro.quantite}" autocomplete="off" size="2">
                                            <a class="cart_quantity_down" href="${pageContext.request.contextPath}/Panier?quantite=${pro.quantite}&idProduit=${pro.p.idProduit}&action=restar"> - </a>
                                        </div>
                                    </td>

                                    <td class="cart_total">
                                        <p id="precio_1" class="cart_total_price">
                                            <fmt:setLocale value="fr_CA"/>
                                            <fmt:formatNumber value="${pro.p.prix*pro.quantite}" type="currency"/>

                                        </p>
                                    </td>
                                    <td class="cart_delete">
                                        <a class="cart_quantity_delete" href="${pageContext.request.contextPath}/Panier?action=delete&idProduit=${pro.p.idProduit}"><i class="fa fa-times"></i></a>
                                    </td>
                                </tr>
                            </c:forEach>

                        </tbody>
                    </table>
                </div>
            </div>
        </section> <!--/#cart_items-->

        <section id="do_action">
            <div class="container">
                <div class="heading">
                    <h3>Que voudrais-tu faire maintenant ?</h3>

                </div>
                <div class="row">

                    <div class="col-sm-10">
                        <div class="total_area">
                            <ul>
                                <fmt:setLocale value="fr_CA"/>
                                <li>Sous-total <span><fmt:formatNumber value="${total}" type="currency"/>CAD</span></li>							
                                <li>TSP/TVQ (15%) <span><fmt:formatNumber value="${total*0.15}" type="currency"/>CAD</span></li>                                                       
                                <li><h3>Total <span><fmt:formatNumber value="${total+(total*0.15)}" type="currency"/>CAD</span></h3></li>
                            </ul>
                            <a class="btn btn-default update" href="Controlleur">Poursuivre vos achats</a>

                            <%if (session.getAttribute("nomUtilisateur") == null) {%>
                           
                            <a class="btn btn-default check_out" href="#" disabled >Passer la commande</a>
                            <%} else {%>
                            <a class="btn btn-default check_out" href="#"  >Passer la commande</a>     <%}%>
                            <hr>

                        </div>
                    </div>
                </div>
            </div>
        </section>

    </body>


    <!--Footer-->
    <jsp:include page="/WEB-INF/commons/footer.jsp"/>

    <!--Multi-Modal-->
    <jsp:include page="/WEB-INF/forms/multi_modal.jsp"/>

    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.scrollUp.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <script src="js/jquery.prettyPhoto.js"></script>
    <script src="js/main.js"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</body>
</html>
