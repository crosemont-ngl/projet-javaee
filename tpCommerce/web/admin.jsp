<%-- 
    Document   : admin
    Created on : 2020-11-30, 11:53:24
    Author     : Nadia Garcia
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>E-KUSA | Administration </title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
    </head>
    <body>    

        <!--Nav admin-->
        <jsp:include page="/WEB-INF/inclus_admin/nav_admin.jsp"/>

        <br><br>
        <!--Liste Animals-->
        <jsp:include page="/WEB-INF/inclus_admin/listeAnimals.jsp"/>
        <!--Liste Produits-->
        <jsp:include page="/WEB-INF/inclus_admin/listeProduits.jsp"/>        

        <script>
            <jsp:include page="js/filtre_admin.js"/>
        </script>

    </body>
</html>
