     <div class="container mt-3">
          
            <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
            <section id="produits">

                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <hr>
                            <h2>Produits</h2>
                            <br>
                            <input class="form-control" id="myInput" type="text" placeholder="Search..">
                            <br>
                            <br>
                            <div class="table-striped">
                                <table class="table table table-bordered">
                                    <thead  class="thead-dark">
                                        <tr>
                                            <th>N� </th>
                                            <th>Nom produit</th>
                                            <th>Prix</th>
                                            <th>Description</th>
                                            <th>Nom animal</th> 
                                            <th colspan="2"> <a href="${pageContext.request.contextPath}/Controlleur?action=afficherFormCreerProduit" class="btn btn-default btn-sm btn-plus" >
                                                    <span class="fa fa-plus-square fa-2x " style="color:red; margin-left: 35px"></span>
                                                </a></th>

                                        </tr>
                                    </thead>
                                    <tbody id="myTable">
                                        <c:forEach var="produit" items="${produits}" varStatus="status">

                                            <tr>
                                                <td>${status.count}</td>
                                                <td>${produit.nomProduit}</td>
                                                <td>${produit.prix}</td>
                                                <td>${produit.description}</td>

                                                <td>${produit.nomAnimal}</td>

                                                <td><a href="${pageContext.request.contextPath}/Controlleur?action=afficherFormEditerProduit&idProduit=${produit.idProduit}" class="btn btn-default btn-sm btn-edit">
                                                        <span class=" fas fa-edit  fa-lg"></span>
                                                    </a>
                                                </td>
                                                <td><a href="${pageContext.request.contextPath}/Controlleur?action=supprimirProduit&idProduit=${produit.idProduit}" class="btn btn-default btn-sm btn-del">
                                                        <span class="fa fa-trash fa-lg"></span> 
                                                    </a>
                                                </td>
                                            </tr>

                                        </c:forEach>

                                    </tbody>
                                </table>
                            </div>                         
                        </div>
                    </div>
                </div>

            </section>


           
        </div>