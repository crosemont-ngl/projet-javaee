<header id="header"><!--header-->

    <div class="header-middle"><!--header-middle-->
        <div class="container">
            <%
                if (request.getAttribute("msg") != null) {
            %> 
            <div  class=" float-center alert alert-<%=request.getAttribute("typemsg").toString().equals("error") ? "danger" : "primary"%>" role="alert">
                <span class="closebtn" onclick="this.parentElement.style.display = 'none';">&times;</span> 
                <%= request.getAttribute("msg")%>
            </div>
            <%
                }%> 
                
            <div class="row">
                <div class="col-sm-4">
                    <div class="logo pull-left">
                        <a href="admin.jsp"><img src="images/home/logo.png" alt="" /></a>
                    </div>
                </div>


                <div class="col-sm-8">             

                    <div class="mainmenu pull-right" style="margin-top:30px">
                        <ul class="nav navbar-nav">
                            <%if (session.getAttribute("nomUtilisateur") == null) {%>
                            <li><a href="${pageContext.request.contextPath}/Controlleur?action=afficherFormLogin"><i class="fa fa-user"></i>Se connecter</a></li>
                                <%} else {%> 
                            <li><a href="#">Bonjour Admin, ${nomUtilisateur}</a></li> 
                            <li><a href="${pageContext.request.contextPath}/Controlleur?action=logout"><i class="fa fa-user"></i> Se déconnecter</a></li>                             
                            <li><a href="accueil.jsp" class="active">Voir affichage produits >></a></li>
                           <%}%>
                        </ul>
                    </div>
                </div> 

            </div>
        </div>
    </div><!--/header-middle-->

    <div class="header-bottom"><!--header-bottom-->
        <div class="container">
            <div class="row">
                <div class="col-sm-9">
                    
                </div>
                <div class="col-sm-3">
                    
                </div>

            </div>
        </div>
    </div>
</header><!--/header-->