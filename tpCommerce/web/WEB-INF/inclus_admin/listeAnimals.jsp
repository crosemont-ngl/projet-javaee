<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


<section id="animals">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2>Animals</h2>
                <br>
                <div>
                    <div class="table-responsive-sm">          
                        <table class="table table-striped" style="width: 50%">
                            <thead class="thead-dark">
                                <tr>
                                    <th>N�</th>
                                    <th>Animal</th>
                                    <th colspan="2" > 
                                        <form class="form-inline" action="${pageContext.request.contextPath}/Controlleur?action=ajouterAnimal"
                                              method="POST" >
                                            <div class="form-group">
                                                <input type="text" class="form-control" placeholder="Ajouter animal" name="nomAnimal" required>
                                            </div> 
                                            <button class="btn " type="submit" >  <span class="fa fa-plus-square fa-2x " style="color:red; margin-left: 25px"></span> </button>
                                        </form>

                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <c:forEach var="animal" items="${animals}" varStatus="status">
                                    <tr>
                                        <td>${status.count}</td>
                                        <td>${animal.nomAnimal}</td>
                                        <td ><a href="${pageContext.request.contextPath}/Controlleur?action=afficherFormEditerAnimal&idAnimal=${animal.idAnimal}" class="btn btn-default btn-sm btn-edit">
                                                <span class=" fas fa-edit  fa-lg"></span>
                                            </a>                                      
                                        </td>
                                        <td > <a href="${pageContext.request.contextPath}/Controlleur?action=eliminerAnimal&idAnimal=${animal.idAnimal}" class="btn btn-default btn-sm btn-del">
                                                  <span class="fa fa-trash fa-lg"></span> 
                                            </a>
                                        </td>
                                    </tr>
                                </c:forEach>
                            </tbody>
                        </table>
                    </div>                 
                </div>
            </div>

        </div>
    </div>
</section>

