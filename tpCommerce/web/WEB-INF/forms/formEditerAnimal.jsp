<%-- 
    Document   : formEditerAnimal
    Created on : 2020-11-28, 00:21:14
    Author     : Nadia Garcia
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>E-KUSA |  La boutique pour vous et votre famille</title>
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/font-awesome.min.css" rel="stylesheet">
        <link href="css/prettyPhoto.css" rel="stylesheet">
        <link href="css/animate.css" rel="stylesheet">
        <link href="css/main.css" rel="stylesheet">
        <link href="css/responsive.css" rel="stylesheet">      
        <link rel="shortcut icon" href="images/icono.png">
        <link rel="icon" href="images/icono.png" type="image/x-icon">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
        <link href="css/formAnimal.css" rel="stylesheet">
        <script src="https://kit.fontawesome.com/f90d3bf50d.js" crossorigin="anonymous"></script>
        

    </head>
    <body>
        <!--Header Admin-->
        <jsp:include page="../inclus_admin/header_admin.jsp"/>
        <div class="container">
            <h2>Formulaire modifier animal:</h2>  
            <br><br>
            <form action="${pageContext.request.contextPath}/Controlleur?action=modifierAnimal&idAnimal=${animal.idAnimal}" method="POST" class="card">
                <div class="form-group">
                    <label for="idAnimal">idAnimal</label>
                    <input type="text" class="form-control"   name="idAnimal" value="${animal.idAnimal}"disabled>
                </div>
                <div class="form-group">
                    <label for="nomAnimal">nomAnimal</label>
                    <input type="text" class="form-control" name="nomAnimal"  value="${animal.getNomAnimal()}" required>
                </div>
                <button class="btn  btn-warning" type="submit"> <span class="glyphicon glyphicon-plus"></span>Modifier </button>
            </form>
        </div>

    </body>
</html>
