<%-- 
    Document   : formCreerProduit
    Created on : 2020-11-29, 19:59:31
    Author     : Nadia Garcia
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>E-KUSA |  Gestion de produits</title>
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/font-awesome.min.css" rel="stylesheet">
        <link href="css/prettyPhoto.css" rel="stylesheet">
        <link href="css/animate.css" rel="stylesheet">
        <link href="css/main.css" rel="stylesheet">
        <link href="css/responsive.css" rel="stylesheet">   
        <link href="css/formProduit.css" rel="stylesheet">  
        <link rel="shortcut icon" href="images/icono.png">
        <link rel="icon" href="images/icono.png" type="image/x-icon">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
        <script src="https://kit.fontawesome.com/f90d3bf50d.js" crossorigin="anonymous"></script>
        <!--Ajout pour le modal-->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round">
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

        <style>
        
        </style>
    </head>
    <body>
        <!--Header Admin-->
        <jsp:include page="/WEB-INF/inclus_admin/header_admin.jsp"/>
        <div class="container">    
            <h3>Formulaire de création du produit:</h3>
            <br>  <br>
            <form  action="${pageContext.request.contextPath}/Controlleur?action=ajouterProduit" method="POST"  class="card" > 
                <div class="row ">
                    <div class="col-sm-3">
                        <label for="nomProd" class="mb-2 mr-sm-2">Nom Product :</label>
                        <input class="form-control mb-2 mr-sm-2" id="nomProd" type="text" name="nomProduit" placeholder="Nom produit"  required />
                    </div>
                    <div class="col-sm-3">
                        <label for="prixProd" class="mb-2 mr-sm-2">Prix :</label>
                        <input class="form-control mb-2 mr-sm-2" id="prixProd" type="text" name="prix" placeholder="Prix" required />
                    </div>
                    <div class="col-sm-3"> 
                        <label for="nomAni" class="mb-2 mr-sm-2">Nom animal </label>
                        <select  class="form-control mb-3" name="idAnimal" id="nomAni" required>
                            <option value="">Sélectionnez un animal</option>
                            <c:forEach var="animal" items="${animals}" varStatus="status">
                                <option value="${animal.idAnimal}"> ${animal.nomAnimal} </option>
                            </c:forEach>
                        </select>
                    </div> 
                    <div class="col-sm-3"> 
                        <label for="telecharger"class="mb-2 mr-sm-2">Sélectionner une image</label>
                        <input id="telecharger"type="file" name="image" value="Sélectionner une image" required />
                    </div>

                </div>
                <br>
                <div class="row ">

                    <div class="col-sm-4">
                        <label for="descript" class="mb-2 mr-sm-2">Description produit</label>                       
                        <input class="form-control mb-2 mr-sm-2" id="descript" type="text" name="description" placeholder="Description" required />
                    </div>
                    <div class="col-sm-2">
                        <label for="demande" class="mb-2 mr-sm-2">Produit demandé</label>                       
                        <input class="form-control mb-2 mr-sm-2" id="demande" type="number" name="produitDemande"  value="${produit.produitDemande}" min="0" max="1" title="0 = produit non demandé"/>
                    </div>
                    <div class="col-sm-2">
                        <label for="suggere" class="mb-2 mr-sm-2">Produit suggeré</label>                       
                        <input class="form-control mb-2 mr-sm-2" id="suggere" type="number" name="produitSuggere"  value="${produit.produitSuggere}" min="0" max="2" title="0 = produit non suggeré"/>
                    </div>

                </div>
                <br>
                <br>
                <input class="btn btn-warning"  type="submit" value="Inserer" />
            </form>
        </div>  
    </body>
</html>
