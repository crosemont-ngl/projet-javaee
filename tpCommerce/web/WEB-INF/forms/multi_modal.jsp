<div class="modal" id="modalLRForm"><!-- DEBUT modal -->
    <div class="modal-dialog"> <!-- DEBUT modal-dialog -->
        <div class="modal-content"><!-- DEBUT modal-content -->


            <div class="modal-header"  >   <!-- DEBUT modal header -->
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h5 class="modal-title">
                    <ul class="nav nav-tabs  " role="tablist"   >
                        <li class="nav-item" >
                            <a class="nav-link active " data-toggle="tab" href="#panel7" role="tab" >Se connecter</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#panel8" role="tab" >S'inscrire</a>
                        </li>
                    </ul>
                </h5>

            </div><!-- FINAL modal header -->


            <div class="tab-content"><!--DEBUT tab-content -->

                <div class="tab-pane fade in show active" id="panel7" role="tabpanel"> <!--DEBUT Panel 7-->
                    <div class="modal-body"> <!-- DEBUT body panel-7-->
                        <form  method="POST" class="register-form was-validated" id="login-form" action="${pageContext.request.contextPath}/Controlleur?action=login" >
                            <div class="form-group">
                                <h5>Adresse Courriel</h5>
                                <input type="email" class="form-control" id="email" placeholder="Adresse Courriel" name="email">
                            </div>
                            <div class="form-group">
                                <h5>Mot de passe</h5>
                                <input type="password" class="form-control" id="pwd" placeholder="Mot de passe" name="motPasse">
                            </div>

                            <div class="modal-footer">
                                <input type="submit" class="btn btn-danger" value="Se connecter">
                            </div>
                        </form><!--.login-form-->

                    </div><!-- FINAL body panel-7-->
                </div> <!--FINAL Panel 7-->


                <div class="tab-pane fade" id="panel8" role="tabpanel">  <!-- DEBUT Panel 8-->


                    <div class="modal-body"> <!-- DEBUT body panel-8-->
                        <form class="register-form was-validated" action="${pageContext.request.contextPath}/Controlleur?action=inscription" method="POST">
                            <div class="form-group">
                                <h5>Nom utilisateur</h5>
                                <input type="text" class="form-control" id="user_name" autocomplete="off" placeholder="Nom utilisateur" name="nomUtilisateur" required>
                            </div>
                            <div class="form-group">
                                <h5>Adresse Courriel</h5>
                                <input type="email" class="form-control" id="user_email" autocomplete="off" placeholder="Adresse Courriel" name="email" required>
                            </div>

                            <div class="form-group">
                                <h5>Mot de passe</h5>
                                <!--input type="password" class="form-control" id="user_pass" autocomplete="off" placeholder="Mot de passe" name="motPasse" required-->
                                <input type="password" class="form-control" id="user_pass" autocomplete="off" placeholder="Mot de passe" name="motPasse" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" 
                                       title="Doit contenir au moins un chiffre et une lettre majuscule et minuscule, et au moins 8 caractères ou plus" required>
                            </div>

                            <div class="form-group">
                                <input type="hidden" class="form-control" id="type_user"  name="typeUtilisateur" value="NA">
                                
                            </div>

                            <div class="modal-footer">
                                <input type="submit" class="btn btn-danger" value="S'inscrire">
                            </div>
                        </form>
                    </div><!-- FINAL body panel-8-->

                </div>  <!-- FINAL Panel 8-->


            </div><!--FINAL tab-content -->



        </div><!-- FINAL modal-content -->
    </div><!-- FINAL modal-dialog -->
</div><!-- FINAL Modal -->