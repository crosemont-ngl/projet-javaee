<%@page import="com.model.entities.Animal"%>
<%@page import="com.model.entities.Item"%>
<%@page import="java.util.ArrayList"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
        <% ArrayList<Item> cart = (ArrayList<Item>) session.getAttribute("cart");%>
        <% Animal animal = (Animal) request.getAttribute("animal");%>
        <c:forEach items="${sessionScope.cart}" var="pro">
           <c:set  var="total" value="${total+pro.p.prix*pro.quantite}"/>         
        </c:forEach>
        
        <header id="header">

            <div class="header-middle">
                <div class="container">
                    <%
                        if (request.getAttribute("msg") != null) {
                    %> 
                    <div  class=" float-center alert alert-<%=request.getAttribute("typemsg").toString().equals("error") ? "danger" : "primary"%>" role="alert">
                        <span class="closebtn" onclick="this.parentElement.style.display = 'none';">&times;</span> 
                        <%= request.getAttribute("msg")%>
                    </div>
                    <%
                        }%> 


                    <div class="row">
                        <div class="col-sm-4">
                            <div class="logo pull-left">
                                <a href="accueil.jsp"><img src="images/home/logo.png" alt="" /></a>
                            </div>
                        </div>

                      
                        <div class="col-sm-8" >

                         
                            <div class="mainmenu pull-right" style="margin-top:30px">
                                <ul class="nav navbar-nav">
                                    <%if (session.getAttribute("nomUtilisateur") == null) {%>
                                    <li><a href="#"data-toggle="modal" data-target="#modalLRForm"><i class="fa fa-user"></i>  Se connecter</a></li>                                     
                                 <fmt:setLocale value="fr_CA"/>
                                    <li><a href="cart.jsp"><i class="fa fa-shopping-cart" ></i>($ <span style="color:#FF0040">
                                                <fmt:formatNumber value="${total}" type="currency"/>CAD</span>) </a></li>
                               
                                      <%} else {%>
                                    <%if (session.getAttribute("typeUtilisateur").equals("A")) {%>
                                    <li><a href="${pageContext.request.contextPath}/Controlleur?action=afficherContenuAdmin"class="active"> << Retourner page admin</a></li>
                                    <li><a href="#">Bonjour Admin, ${nomUtilisateur}</a></li>
                                    <li><a href="${pageContext.request.contextPath}/Controlleur?action=logout"><i class="fa fa-user"></i>  Se déconnecter</a></li>
                                        <%} else {%>                

                                    <li class="dropdown"><a href="#">Bonjour, ${nomUtilisateur}<i class="fa fa-angle-down"></i></a>
                                        <ul role="menu" class="sub-menu">
                                            <li><a href="#">Votre compte</a></li>                                           
                                            <li><a href="${pageContext.request.contextPath}/Controlleur?action=logout">  Se déconnecter</a></li> 
                                        </ul>
                                    </li>
                                    <fmt:setLocale value="fr_CA"/>
                                    <li><a href="cart.jsp"><i class="fa fa-shopping-cart" ></i>($<span style="color:#FF0040"> 
                                                <fmt:formatNumber value="${total}" type="currency"/>CAD</span>) </a></li>
                                    <%}%>
                                    <%}%>
                           
                                </ul>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <div class="header-bottom">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-9">
                          

                            <div class="mainmenu pull-left">
                                <ul class="nav navbar-nav collapse navbar-collapse">
                                    <li><a href="#" class="active">Accueil</a></li>
                                    <li><a href="#">Nos produits</a></li>
                                    <li><a href="#">Promotions</a></li> 
                                    <li><a href="#">Club Patte Douce</a></li>
                                    <li><a href="#">Nous contacter</a></li>
                                </ul>
                            </div>

                        </div>
                        <div class="col-sm-3">
                            <div class="search_box pull-right">
                                <input type="text" placeholder="Rechercher..."/>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </header>