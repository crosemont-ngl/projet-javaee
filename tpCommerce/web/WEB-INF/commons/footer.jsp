<footer id="footer"><!--Footer-->
    <div class="footer-top">
        <div class="container">
            <div class="row">
                <div class="col-sm-2">
                    <div class="companyinfo">
                        <h2><span>E</span>-KUSA</h2>
                        <p>Visitez nos succursales et b�n�ficiez de remises sp�ciales avant la rupture de stock.</p>
                    </div>
                </div>

                <div class="col-sm-2">
                    <div class="single-widget">
                        <h2>Services</h2>
                        <ul class="nav nav-pills nav-stacked">
                            <li><a href="#">Coupe-griffes</a></li>
                            <li><a href="#">Lave-toutou</a></li>
                            <li><a href="#">Zone adoption</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="single-widget">
                        <h2>Aide</h2>
                        <ul class="nav nav-pills nav-stacked">
                            <li><a href="#">Condition d'utilisation</a></li>
                            <li><a href="#">Politique de confidentialit�</a></li>
                            <li><a href="#">Politique de remboursement</a></li>

                        </ul>
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="single-widget">
                        <h2>� propos</h2>
                        <ul class="nav nav-pills nav-stacked">
                            <li><a href="#">Histoire</a></li>
                            <li><a href="#">Profil et mission</a></li>
                            <li><a href="#">Club Patte Douce</a></li>
                        </ul>
                    </div>
                </div>


                <div class="col-sm-3">
                    <div class="address">
                        <img src="images/home/map.png" alt="" />
                        <p>Acheter en ligne sans quitter la maison</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="footer-bottom">
        <div class="container">
            <div class="row">
                <p class="pull-left">Copyright � 2020 .Tous droits r�serv�s.</p>
                <p class="pull-right">Site web  cr�� par <span><a>Nadia Garcia</a></span></p>
            </div>
        </div>
    </div>

</footer><!--/Footer-->