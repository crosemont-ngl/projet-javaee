package com.control;

import com.model.dao.AnimalDAO;
import com.model.dao.ProduitDAO;
import com.model.dao.UtilisateurDAO;
import com.model.entities.Animal;
import com.model.entities.Produit;
import com.model.entities.Utilisateur;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Nadia Garcia
 */
public class Controlleur extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String action = request.getParameter("action");
        if (action != null) {
            switch (action) {
                case "accueil":
                    System.out.println("afficher produits");
                    break;
                case "afficherContenuAdmin": {
                    try {
                        this.afficherContenuAdmin(request, response);
                    } catch (ClassNotFoundException ex) {
                       ex.printStackTrace(System.out);
                    }
                }
                break;
                case "afficherFormLogin":
                    this.afficherFormLogin(request, response);
                    break;
                case "afficherFormInscription":
                    this.afficherFormInscription(request, response);
                    break;
                case "logout": {
                    try {
                        this.sedeconnecter(request, response);
                    } catch (ClassNotFoundException ex) {
                        ex.printStackTrace(System.out);
                    }
                }
                break;
                case "afficherAllCategories": {
                    try {
                        this.afficherAllCategories(request, response);
                    } catch (ClassNotFoundException ex) {
                        ex.printStackTrace(System.out);
                    }
                }
                break;
                case "afficherFormEditerAnimal":
                    this.afficherFormEditerAnimal(request, response);
                    break;
                case "eliminerAnimal": {
                    try {
                        this.eliminerAnimal(request, response);
                    } catch (SQLException ex) {
                        ex.printStackTrace(System.out);
                    } catch (ClassNotFoundException ex) {
                        ex.printStackTrace(System.out);
                    }
                }
                break;
                case "afficherFormEditerProduit":
                    this.afficherFormEditerProduit(request, response);
                    break;
                case "afficherFormCreerProduit":
                    this.afficherFormCreerProduit(request, response);
                    break;

                case "supprimirProduit": {
                    try {
                        this.supprimirProduit(request, response);
                    } catch (SQLException ex) {
                        ex.printStackTrace(System.out);
                    } catch (ClassNotFoundException ex) {
                        ex.printStackTrace(System.out);
                    }
                }
                break;
                case "listerProduitsSelectiones": {
                    try {
                        this.listerProduitsSelectiones(request, response);
                    } catch (ClassNotFoundException ex) {
                        ex.printStackTrace(System.out);
                    }
                }
                break;

                default: {
                    try {
                        this.actionDefault(request, response);
                    } catch (ClassNotFoundException ex) {
                        ex.printStackTrace(System.out);
                    }
                }
            }
        } else {
            try {
                
                this.actionDefault(request, response);
            } catch (ClassNotFoundException ex) {
                ex.printStackTrace(System.out);
            }
        }

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = request.getParameter("action");
        if (action != null) {
            switch (action) {
                case "inscription":
            {
                try {
                    this.inscrireUtilisateur(request, response);
                } catch (ClassNotFoundException ex) {
                     ex.printStackTrace(System.out);
                } catch (SQLException ex) {
                   ex.printStackTrace(System.out);
                }
            }
                    break;
                case "login": {
                    try {
                        this.seconecter(request, response);
                    } catch (SQLException ex) {
                        ex.printStackTrace(System.out);
                    } catch (ClassNotFoundException ex) {
                        ex.printStackTrace(System.out);
                    }
                }
                break;
                case "ajouterAnimal": {
                    try {
                        this.ajouterAnimal(request, response);
                    } catch (SQLException ex) {
                        ex.printStackTrace(System.out);
                    } catch (ClassNotFoundException ex) {
                        ex.printStackTrace(System.out);
                    }
                }
                break;
                case "modifierAnimal": {
                    try {
                        this.modifierAnimal(request, response);
                    } catch (SQLException ex) {
                        ex.printStackTrace(System.out);
                    } catch (ClassNotFoundException ex) {

                    }
                }
                break;

                case "gestionProduit":
                    this.gestionProduit(request, response);
                    break;

                case "modifierProduit": {
                    try {
                        this.modifierProduit(request, response);
                    } catch (SQLException ex) {
                        ex.printStackTrace(System.out);
                    } catch (ClassNotFoundException ex) {
                        ex.printStackTrace(System.out);
                    }
                }
                break;
                case "ajouterProduit": {
                    try {
                        this.ajouterProduit(request, response);
                    } catch (SQLException ex) {
                        ex.printStackTrace(System.out);
                    } catch (ClassNotFoundException ex) {
                        ex.printStackTrace(System.out);
                    }
                }
                break;
                default: {
                    try {
                        this.actionDefault(request, response);
                    } catch (ClassNotFoundException ex) {
                        ex.printStackTrace(System.out);
                    }
                }

            }
        } else {
            try {
                this.actionDefault(request, response);
            } catch (ClassNotFoundException ex) {
                ex.printStackTrace(System.out);
            }
        }

    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private void actionErreur(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.sendRedirect("404.jsp");
    }

    private void actionDefault(HttpServletRequest request, HttpServletResponse response) throws IOException, ClassNotFoundException {
        HttpSession session = request.getSession();

        List<Produit> produits = new ProduitDAO().listerProduitsDemandes();
        session.setAttribute("produits", produits);

        List<Animal> animals = new AnimalDAO().selectionnerALL();

        List<Produit> productsActive = new ProduitDAO().listerProduitsSuggeresACTIVE();
        List<Produit> productsItem = new ProduitDAO().listerProduitsSuggeresITEM();
        session.setAttribute("animals", animals);

        session.setAttribute("productsActive", productsActive);
        session.setAttribute("productsItem", productsItem);
        response.sendRedirect("accueil.jsp");
    }

    private void afficherFormLogin(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.sendRedirect("formLogin.jsp");
    }

    private void afficherFormInscription(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.sendRedirect("formInscription.jsp");
    }

    private void inscrireUtilisateur(HttpServletRequest request, HttpServletResponse response) throws IOException, ClassNotFoundException, ServletException, SQLException {
        HttpSession session = request.getSession();
        String nomUtilisateur = request.getParameter("nomUtilisateur");
        String email = request.getParameter("email");
        String motPasse = request.getParameter("motPasse");
        String typeUtilisateur = request.getParameter("typeUtilisateur");       
         
        Utilisateur  utilisateur = new Utilisateur(nomUtilisateur,email,motPasse,typeUtilisateur);
        int enregistrementCree = new UtilisateurDAO().creerUtilisateur(utilisateur);
        this.seconecter(request, response);

    }

    private void seconecter(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, SQLException, ClassNotFoundException {

        HttpSession session = request.getSession();
        String email = request.getParameter("email");
        String motPasse = request.getParameter("motPasse");
        Utilisateur u = new UtilisateurDAO().trouverUtilisateur(new Utilisateur(email, motPasse));

        session.setAttribute("u", u);
        System.out.println(u);
        if (u.getIdUtilisateur() != 0) {
            session.setAttribute("nomUtilisateur", u.getNomUtilisateur());
            session.setAttribute("typeUtilisateur", u.getTypeUtilisateur());
            if (u.getTypeUtilisateur().equals("A")) {
                System.out.println("es admin");
                this.afficherContenuAdmin(request, response);

            } else {
                System.out.println("no eres admin");
                request.getRequestDispatcher("accueil.jsp").forward(request, response);
            }

        } else {
            request.setAttribute("msg", "Le nom d'utilisateur ou mot de passe invalide");
            request.setAttribute("typemsg", "erreur");
            request.getRequestDispatcher("accueil.jsp").forward(request, response);

        }

    }

    private void sedeconnecter(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException, ClassNotFoundException {
        HttpSession session = request.getSession(true);
        if (session != null) {
            session.invalidate();
           
            this.actionDefault(request, response);
        }
    }

    private void afficherAllCategories(HttpServletRequest request, HttpServletResponse response) throws ClassNotFoundException, IOException {
        List<Animal> animals = new AnimalDAO().selectionnerALL();
        HttpSession sesion = request.getSession();
        sesion.setAttribute("animals", animals);
        
    }

    private void afficherContenuAdmin(HttpServletRequest request, HttpServletResponse response) throws ClassNotFoundException, IOException, ServletException {
        HttpSession sesion = request.getSession();
        List<Animal> animals = new AnimalDAO().selectionnerALL();
        List<Produit> produits = new ProduitDAO().selectionnerALL();
        sesion.setAttribute("animals", animals);
        sesion.setAttribute("produits", produits);
        request.getRequestDispatcher("admin.jsp").forward(request, response);
       
    }

    private void afficherAllProduits(HttpServletRequest request, HttpServletResponse response) {
        HttpSession sesion = request.getSession();
        List<Produit> produits = new ProduitDAO().selectionnerALL();
        sesion.setAttribute("produits", produits);
    }

    private void afficherFormEditerAnimal(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        int idAnimal = Integer.parseInt(request.getParameter("idAnimal"));
       
        Animal animal = new AnimalDAO().trouverAnimal(new Animal(idAnimal));
        
        request.setAttribute("animal", animal);
        request.getRequestDispatcher("/WEB-INF/forms/formEditerAnimal.jsp").forward(request, response);
    }

    private void ajouterAnimal(HttpServletRequest request, HttpServletResponse response) throws SQLException, ServletException, IOException, ClassNotFoundException {
        String nomAnimal = request.getParameter("nomAnimal");
        Animal animal = new Animal(nomAnimal);
        int row = new AnimalDAO().creerAnimal(animal);
        this.actionDefault(request, response);
    }

    private void modifierAnimal(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException, ClassNotFoundException {
        int idAnimal = Integer.parseInt(request.getParameter("idAnimal"));
        String nomAnimal = request.getParameter("nomAnimal");
        Animal animal = new Animal(idAnimal, nomAnimal);
        int registrosModificados = new AnimalDAO().modifierAnimal(animal);
        
        this.actionDefault(request, response);

    }

    private void eliminerAnimal(HttpServletRequest request, HttpServletResponse response) throws IOException, SQLException, ClassNotFoundException {
        int idAnimal = Integer.parseInt(request.getParameter("idAnimal"));
        Animal animal = new Animal(idAnimal);
        int registrosModificados = new AnimalDAO().eliminerAnimal(animal);
        System.out.println("registrosModificados = " + registrosModificados);
        this.actionDefault(request, response);
    }

    private void gestionProduit(HttpServletRequest request, HttpServletResponse response) {

    }

    private void ajouterProduit(HttpServletRequest request, HttpServletResponse response) throws SQLException, ServletException, IOException, ClassNotFoundException {
        String nomProduit = request.getParameter("nomProduit");
        double prix = Double.parseDouble(request.getParameter("prix"));
        String description = request.getParameter("description");
        int produitDemande = Integer.parseInt(request.getParameter("produitDemande"));
        String image = request.getParameter("image");
        int idAnimal = Integer.parseInt(request.getParameter("idAnimal"));
        int produitSuggere = Integer.parseInt(request.getParameter("produitSuggere"));
        Produit produit = new Produit(nomProduit, prix, description, produitDemande, image, idAnimal, produitSuggere);
        int row = new ProduitDAO().creerProduit(produit);
       
        this.afficherAllProduits(request, response);
        request.getRequestDispatcher("admin.jsp").forward(request, response);

    }

    private void modifierProduit(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException, ClassNotFoundException {
        int idProduit = Integer.parseInt(request.getParameter("idProduit"));
        String nomProduit = request.getParameter("nomProduit");
        double prix = Double.parseDouble(request.getParameter("prix"));
        String description = request.getParameter("description");
        int produitDemande = Integer.parseInt(request.getParameter("produitDemande"));
        String image = request.getParameter("image");
        int idAnimal = Integer.parseInt(request.getParameter("idAnimal"));
        int produitSuggere = Integer.parseInt(request.getParameter("produitSuggere"));

        Produit produit = new Produit(idProduit, nomProduit, prix, description, produitDemande, image, idAnimal, produitSuggere);
        int registrosModificados = new ProduitDAO().modifierProduit(produit);
        System.out.println("producto modificado desde controlador " + registrosModificados);
        //this.actionDefault(request, response);
        response.sendRedirect("admin.jsp");
        //request.getRequestDispatcher("admin.jsp").forward(request, response);

        

    }

    private void supprimirProduit(HttpServletRequest request, HttpServletResponse response) throws IOException, SQLException, ClassNotFoundException, ServletException {
        int idProduit = Integer.parseInt(request.getParameter("idProduit"));
        Produit produit = new Produit(idProduit);
        int registrosBorrados = new ProduitDAO().eliminerProduit(produit);
       
        this.afficherAllProduits(request, response);
        request.getRequestDispatcher("admin.jsp").forward(request, response);
    }

    private void afficherFormEditerProduit(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int idProduit = Integer.parseInt(request.getParameter("idProduit"));
        
        Produit produit = new ProduitDAO().trouverProduit(new Produit(idProduit));
        
        request.setAttribute("produit", produit);
        request.getRequestDispatcher("/WEB-INF/forms/formEditerProduit.jsp").forward(request, response);
    }

    private void afficherFormCreerProduit(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
         request.getRequestDispatcher("/WEB-INF/forms/formCreerProduit.jsp").forward(request, response);
    }

    private void listerProduitsSelectiones(HttpServletRequest request, HttpServletResponse response) throws IOException, ClassNotFoundException {
        HttpSession session = request.getSession();
        int idAnimal = Integer.parseInt(request.getParameter("idAnimal"));        
        List<Produit> produitsSelectiones = new ProduitDAO().listerProduitsByCategorie(idAnimal);        
        session.setAttribute("produitsSelectiones", produitsSelectiones);
        this.actionDefault(request, response);

    }

}
