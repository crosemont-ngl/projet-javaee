/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.control;

import com.model.dao.ProduitDAO;
import com.model.entities.Item;
import com.model.entities.Produit;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Nadia Garcia
 */
public class Panier extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        if (request.getParameter("action") != null) {
            String a = request.getParameter("action");
           
            int idProduit = 0;
            Produit p;
            HttpSession session = request.getSession();
            if (a.equals("order")) {
                idProduit = Integer.parseInt(request.getParameter("idProduit"));
                if (session.getAttribute("cart") == null) {
                    ArrayList<Item> cart = new ArrayList<>();
                    p = ProduitDAO.consulterProduct(idProduit);
                    cart.add(new Item(p, 1));
                    session.setAttribute("cart", cart);
                } else {
                    ArrayList<Item> cart = (ArrayList<Item>) session.getAttribute("cart");
                    int indice = produitExist(idProduit, cart);
                    if (indice == -1) {
                        p = ProduitDAO.consulterProduct(idProduit);
                        cart.add(new Item(p, 1));
                    } else {
                        int cantidad = cart.get(indice).getQuantite() + 1;
                        cart.get(indice).setQuantite(cantidad);
                    }

                    session.setAttribute("cart", cart);
                }
            } else if (a.equals("delete")) {
                idProduit = Integer.parseInt(request.getParameter("idProduit"));
                ArrayList<Item> cart = (ArrayList<Item>) session.getAttribute("cart");
                int indice = produitExist(idProduit, cart);
                cart.remove(indice);
                
                session.setAttribute("cart", cart);
                request.getRequestDispatcher("cart.jsp").forward(request, response);
            } else if (a.equals("finish")) {
                ArrayList<Item> cart = (ArrayList<Item>) session.getAttribute("cart");
                int indice = produitExist(idProduit, cart);
                cart.clear();
                session.setAttribute("cart", cart);
                response.setContentType("text/html;charset=UTF-8");
                request.getRequestDispatcher("Controlleur").forward(request, response);
            } else if (a.equals("restar")) {
                int quantite = Integer.parseInt(request.getParameter("quantite"));
                int id = Integer.parseInt(request.getParameter("idProduit"));
                ArrayList<Item> cart = (ArrayList<Item>) session.getAttribute("cart");
                int indice = produitExist(id, cart);
                int cantidad = cart.get(indice).getQuantite();
                cantidad = cantidad - 1;
                cart.get(indice).setQuantite(cantidad);

                if (cantidad == 0) {
                    cart.remove(indice);
                }
               
                session.setAttribute("cart", cart);
                request.getRequestDispatcher("cart.jsp").forward(request, response);

            }else if(a.equals("sumar")){
           idProduit = Integer.parseInt(request.getParameter("idProduit"));
                if (session.getAttribute("cart") == null) {
                    ArrayList<Item> cart = new ArrayList<>();
                    p = ProduitDAO.consulterProduct(idProduit);
                    cart.add(new Item(p, 1));
                    session.setAttribute("cart", cart);
                } else {
                    ArrayList<Item> cart = (ArrayList<Item>) session.getAttribute("cart");
                    int indice = produitExist(idProduit, cart);
                    if (indice == -1) {
                        p = ProduitDAO.consulterProduct(idProduit);
                        cart.add(new Item(p, 1));
                    } else {
                        int cantidad = cart.get(indice).getQuantite() + 1;
                        cart.get(indice).setQuantite(cantidad);
                    }

                    session.setAttribute("cart", cart);
                }
                
                 request.getRequestDispatcher("cart.jsp").forward(request, response);
            
            }

        }
        response.setContentType("text/html;charset=UTF-8");
        response.sendRedirect("accueil.jsp");
     
    }

    private int produitExist(int idProduit, ArrayList<Item> cart) {
        for (int i = 0; i < cart.size(); i++) {
            if (cart.get(i).getP().getIdProduit() == idProduit) {

                return i;
            }
        }
        return -1;
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
