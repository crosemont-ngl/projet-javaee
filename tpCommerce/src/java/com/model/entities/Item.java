package com.model.entities;

/**
 *
 * @author Nadia Garcia
 */
public class Item {
    private Produit p;
    private int  quantite;   
 

    public Item() {
    }

    public Item(Produit p, int quantite) {
        this.p = p;
        this.quantite = quantite;
    }

    public Produit getP() {
        return p;
    }

    public void setP(Produit p) {
        this.p = p;
    }

    public int getQuantite() {
        return quantite;
    }

    public void setQuantite(int quantite) {
        this.quantite = quantite;
    }

    @Override
    public String toString() {
        return "Item{" + "p=" + p + ", quantite=" + quantite + '}';
    }

    
  
    
}
