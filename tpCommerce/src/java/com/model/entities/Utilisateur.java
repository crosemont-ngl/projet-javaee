package com.model.entities;

import java.io.Serializable;

/**
 *
 * @author Nadia Garcia
 */
public class Utilisateur implements Serializable {

    private int idUtilisateur;
    private String nomUtilisateur;
    private String email;
    private String motPasse;
    private String typeUtilisateur;

    public Utilisateur() {
    }

    public Utilisateur(int idUtilisateur) {
        this.idUtilisateur = idUtilisateur;
    }

    public Utilisateur(String email, String motPasse) {
        this.email = email;
        this.motPasse = motPasse;
    }

    public Utilisateur(String nomUtilisateur) {
        this.nomUtilisateur = nomUtilisateur;
    }

   
    public Utilisateur(int idUtilisateur, String nomUtilisateur, String email, String motPasse, String typeUtilisateur) {
        this.idUtilisateur = idUtilisateur;
        this.nomUtilisateur = nomUtilisateur;
        this.email = email;
        this.motPasse = motPasse;
        this.typeUtilisateur = typeUtilisateur;
    }

    public Utilisateur(String nomUtilisateur, String email, String motPasse, String typeUtilisateur) {
        this.nomUtilisateur = nomUtilisateur;
        this.email = email;
        this.motPasse = motPasse;
        this.typeUtilisateur = typeUtilisateur;
    }

       
    public int getIdUtilisateur() {
        return idUtilisateur;
    }

    public void setIdUtilisateur(int idUtilisateur) {
        this.idUtilisateur = idUtilisateur;
    }

    public String getNomUtilisateur() {
        return nomUtilisateur;
    }

    public void setNomUtilisateur(String nomUtilisateur) {
        this.nomUtilisateur = nomUtilisateur;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMotPasse() {
        return motPasse;
    }

    public void setMotPasse(String motPasse) {
        this.motPasse = motPasse;
    }

    public String getTypeUtilisateur() {
        return typeUtilisateur;
    }

    public void setTypeUtilisateur(String typeUtilisateur) {
        this.typeUtilisateur = typeUtilisateur;
    }

    @Override
    public String toString() {
        return "Utilisateur{" + "idUtilisateur=" + idUtilisateur + ", nomUtilisateur=" + nomUtilisateur + ", email=" + email + ", motPasse=" + motPasse + ", typeUtilisateur=" + typeUtilisateur + '}';
    }

}
