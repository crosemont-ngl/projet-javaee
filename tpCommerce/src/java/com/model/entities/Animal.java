package com.model.entities;

import java.io.Serializable;

/**
 *
 * @author Nadia Garcia
 */
public class Animal implements Serializable{
    private int idAnimal;
    private String nomAnimal;

    public Animal() {
    }

    public Animal(int idAnimal) {
        this.idAnimal = idAnimal;
    }

    public Animal(String nomAnimal) {
        this.nomAnimal = nomAnimal;
    }
    

    public Animal(int idAnimal, String nomAnimal) {
        this.idAnimal = idAnimal;
        this.nomAnimal = nomAnimal;
    }

    public int getIdAnimal() {
        return idAnimal;
    }

    public void setIdAnimal(int idAnimal) {
        this.idAnimal = idAnimal;
    }

    public String getNomAnimal() {
        return nomAnimal;
    }

    public void setNomAnimal(String nomAnimal) {
        this.nomAnimal = nomAnimal;
    }

    @Override
    public String toString() {
        return "Animal{" + "idAnimal=" + idAnimal + ", nomAnimal=" + nomAnimal + '}';
    }

   
    
    
}
