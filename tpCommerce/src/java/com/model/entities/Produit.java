package com.model.entities;

import java.io.Serializable;

/**
 *
 * @author Nadia Garcia
 */
public class Produit implements Serializable{
    
    private int idProduit;
    private String nomProduit;
    private double prix;
    private String description;
    private int produitDemande;
    private String image;
    private int idAnimal;
    private String nomAnimal;
    private int produitSuggere;

    public Produit() {
    }

  
    public Produit(int idProduit) {
        this.idProduit = idProduit;
    }

    public Produit(int idProduit, String nomProduit, double prix, String description, int produitDemande, String image, int idAnimal, String nomAnimal, int produitSuggere) {
        this.idProduit = idProduit;
        this.nomProduit = nomProduit;
        this.prix = prix;
        this.description = description;
        this.produitDemande = produitDemande;
        this.image = image;
        this.idAnimal = idAnimal;
        this.nomAnimal = nomAnimal;
        this.produitSuggere = produitSuggere;
    }

    public Produit(String nomProduit, double prix, String description, int produitDemande, String image, int idAnimal, int produitSuggere) {
        this.nomProduit = nomProduit;
        this.prix = prix;
        this.description = description;
        this.produitDemande = produitDemande;
        this.image = image;
        this.idAnimal = idAnimal;
        this.produitSuggere = produitSuggere;
    }
    
    

    public Produit(int idProduit, String nomProduit, double prix, String description, int produitDemande, String image, int idAnimal, int produitSuggere) {
        this.idProduit = idProduit;
        this.nomProduit = nomProduit;
        this.prix = prix;
        this.description = description;
        this.produitDemande = produitDemande;
        this.image = image;
        this.idAnimal = idAnimal;
        this.produitSuggere = produitSuggere;
    }

    
  
   public int getIdProduit() {
        return idProduit;
    }

    public void setIdProduit(int idProduit) {
        this.idProduit = idProduit;
    }

    public String getNomProduit() {
        return nomProduit;
    }

    public void setNomProduit(String nomProduit) {
        this.nomProduit = nomProduit;
    }

    public double getPrix() {
        return prix;
    }

    public void setPrix(double prix) {
        this.prix = prix;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

  
    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getIdAnimal() {
        return idAnimal;
    }

    public void setIdAnimal(int idAnimal) {
        this.idAnimal = idAnimal;
    }

    public String getNomAnimal() {
        return nomAnimal;
    }

    public void setNomAnimal(String nomAnimal) {
        this.nomAnimal = nomAnimal;
    }

    public int getProduitDemande() {
        return produitDemande;
    }

    public void setProduitDemande(int produitDemande) {
        this.produitDemande = produitDemande;
    }

    public int getProduitSuggere() {
        return produitSuggere;
    }

    public void setProduitSuggere(int produitSuggere) {
        this.produitSuggere = produitSuggere;
    }

    @Override
    public String toString() {
        return "Produit{" + "idProduit=" + idProduit + ", nomProduit=" + nomProduit + ", prix=" + prix + ", description=" + description + ", produitDemande=" + produitDemande + ", image=" + image + ", idAnimal=" + idAnimal + ", produitSuggere=" + produitSuggere + ", nomAnimal=" + nomAnimal + '}';
    }

    
   
   
    
    
    
    
    
}
