package com.model.dao;

import com.model.entities.Animal;
import com.model.entities.Produit;
import com.service.Connexion;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Nadia Garcia
 */
public class ProduitDAO {

    private static final String SQL_SELECT_ALL = "SELECT id_produit, nom_produit, prix, description_produit, prod_demande, image, produit.id_animal AS id_animal, nom_animal, prod_suggere FROM produit JOIN animal ON produit.id_animal = animal.id_animal";
    private static final String SQL_SELECT_ALL_PRODUIT_BD = "SELECT id_produit, nom_produit, prix, description_produit, prod_demande, image, id_animal, prod_suggere FROM produit";
    private static final String SQL_SELECT_PRODUIT_BY_ID = "SELECT id_produit, nom_produit, prix, description_produit, prod_demande, image, id_animal, prod_suggere FROM produit WHERE id_produit = ?";
    private static final String SQL_SELECT_BY_DEMANDE = "SELECT id_produit, nom_produit, prix, description_produit, image, prod_demande, id_animal, prod_suggere  FROM produit WHERE prod_demande = 1";
    private static final String SQL_SELECT_BY_SUGGERE_ACTIVE = "SELECT id_produit, nom_produit, prix, description_produit, image, prod_demande, id_animal, prod_suggere  FROM produit WHERE prod_suggere = 1";
    private static final String SQL_SELECT_BY_SUGGERE = "SELECT id_produit, nom_produit, prix, description_produit, image, prod_demande, id_animal, prod_suggere  FROM produit WHERE prod_suggere = 2";
    private static final String SQL_SELECT_BY_ANIMAL = "SELECT id_produit, nom_produit, prix, description_produit, image, prod_demande, id_animal, prod_suggere FROM produit WHERE id_animal = ?";
    private static final String SQL_INSERT_PRODUIT = "INSERT INTO produit (nom_produit, prix, description_produit, prod_demande, image, id_animal, prod_suggere ) VALUES (?,?,?,?,?,?,?)";
    private static final String SQL_UPDATE_PRODUIT = "UPDATE produit SET nom_produit = ?, prix = ?, description_produit = ?, prod_demande =?, image = ?, id_animal = ?, prod_suggere =? WHERE id_produit = ?";
    private static final String SQL_DELETE_PRODUIT = "DELETE FROM produit WHERE id_produit = ?";

    public List<Produit> selectionnerALL() {
        Connection cnx = null;
        PreparedStatement stm = null;
        ResultSet rs = null;
        Produit p = null;

        List<Produit> produits = new ArrayList<>();

        try {

            cnx = Connexion.getConnexion();
            stm = cnx.prepareStatement(SQL_SELECT_ALL);
            rs = stm.executeQuery();

            while (rs.next()) {
                int idProduit = rs.getInt("id_produit");
                String nomProduit = rs.getString("nom_produit");
                double prix = rs.getDouble("prix");
                String description = rs.getString("description_produit");
                int prod_demande = rs.getInt("prod_demande");
                String image = rs.getString("image");
                int idAnimal = rs.getInt("id_animal");
                String nomAnimal = rs.getString("nom_animal");
                int prod_suggere = rs.getInt("prod_suggere");

                p = new Produit(idProduit, nomProduit, prix, description, prod_demande, image, idAnimal, nomAnimal, prod_suggere);

                produits.add(p);

            }
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                Connexion.close(rs);
                Connexion.close(stm);
                Connexion.close(cnx);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }

        }
        return produits;

    }

    public int creerProduit(Produit produit) throws SQLException {
        Connection cnx = null;
        PreparedStatement stm = null;
        int rows = 0;
        try {
            cnx = Connexion.getConnexion();
            stm = cnx.prepareStatement(SQL_INSERT_PRODUIT);
            stm.setString(1, produit.getNomProduit());
            stm.setDouble(2, produit.getPrix());
            stm.setString(3, produit.getDescription());
            stm.setInt(4, produit.getProduitDemande());
            stm.setString(5, produit.getImage());
            stm.setInt(6, produit.getIdAnimal());
            stm.setInt(7, produit.getProduitSuggere());
            rows = stm.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            Connexion.close(stm);
            Connexion.close(cnx);
        }
        return rows;
    }

    public int modifierProduit(Produit produit) throws SQLException {
        Connection cnx = null;
        PreparedStatement stm = null;
        int rows = 0;
        try {
            cnx = Connexion.getConnexion();
            stm = cnx.prepareStatement(SQL_UPDATE_PRODUIT);
            stm.setString(1, produit.getNomProduit());
            stm.setDouble(2, produit.getPrix());
            stm.setString(3, produit.getDescription());
            stm.setInt(4, produit.getProduitDemande());
            stm.setString(5, produit.getImage());
            stm.setInt(6, produit.getIdAnimal());
            stm.setInt(7, produit.getProduitSuggere());
            stm.setInt(8, produit.getIdProduit());
            rows = stm.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            Connexion.close(stm);
            Connexion.close(cnx);
        }
        return rows;
    }

    public int eliminerProduit(Produit produit) throws SQLException {
        Connection cnx = null;
        PreparedStatement stm = null;
        int rows = 0;
        try {
            cnx = Connexion.getConnexion();
            stm = cnx.prepareStatement(SQL_DELETE_PRODUIT);
            stm.setInt(1, produit.getIdProduit());

            rows = stm.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            Connexion.close(stm);
            Connexion.close(cnx);
        }
        return rows;
    }

    public Produit trouverProduit(Produit produit) {
        Connection cnx = null;
        PreparedStatement stm = null;
        ResultSet rs = null;

        try {

            cnx = Connexion.getConnexion();
            stm = cnx.prepareStatement(SQL_SELECT_PRODUIT_BY_ID);
            stm.setInt(1, produit.getIdProduit());
            rs = stm.executeQuery();

            while (rs.next()) {

                produit.setIdProduit(rs.getInt("id_produit"));
                produit.setNomProduit(rs.getString("nom_produit"));
                produit.setPrix(rs.getDouble("prix"));
                produit.setDescription(rs.getString("description_produit"));
                produit.setProduitDemande(rs.getInt("prod_demande"));
                produit.setImage(rs.getString("image"));
                produit.setIdAnimal(rs.getInt("id_animal"));
                produit.setProduitSuggere(rs.getInt("prod_suggere"));

            }

        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {

                Connexion.close(rs);
                Connexion.close(stm);
                Connexion.close(cnx);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }

        }

        return produit;
    }

    public List<Produit> listerProduitsDemandes() {
        Connection cnx = null;
        PreparedStatement stm = null;
        ResultSet rs = null;

        List<Produit> produits = new ArrayList<>();

        try {

            cnx = Connexion.getConnexion();
            stm = cnx.prepareStatement(SQL_SELECT_BY_DEMANDE);
            //   stm.setInt(1, produit.getProduitDemande());
            rs = stm.executeQuery();

            while (rs.next()) {
                Produit produit = new Produit();
                
                produit.setIdProduit(rs.getInt("id_produit"));
                produit.setNomProduit(rs.getString("nom_produit"));
                produit.setPrix(rs.getDouble("prix"));
                produit.setDescription(rs.getString("description_produit"));
                produit.setProduitDemande(rs.getInt("prod_demande"));
                produit.setImage(rs.getString("image"));
                produit.setIdAnimal(rs.getInt("id_animal"));
                produit.setProduitSuggere(rs.getInt("prod_suggere"));

                produits.add(produit);

            }
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                Connexion.close(rs);
                Connexion.close(stm);
                Connexion.close(cnx);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }

        }
        return produits;

    }

    public List<Produit> listerProduitsSuggeresACTIVE() {
        Connection cnx = null;
        PreparedStatement stm = null;
        ResultSet rs = null;

        List<Produit> produits = new ArrayList<>();

        try {

            cnx = Connexion.getConnexion();
            stm = cnx.prepareStatement(SQL_SELECT_BY_SUGGERE_ACTIVE);

            rs = stm.executeQuery();

            while (rs.next()) {

                Produit produit = new Produit();
                int idProduit = rs.getInt("id_produit");
                String nomProduit = rs.getString("nom_produit");
                double prix = rs.getDouble("prix");
                String description = rs.getString("description_produit");
                int prod_demande = rs.getInt("prod_demande");
                String image = rs.getString("image");
                int idAnimal = rs.getInt("id_animal");
                int prod_suggere = rs.getInt("prod_suggere");

                produit = new Produit(idProduit, nomProduit, prix, description, prod_demande, image, idAnimal, prod_suggere);

                produits.add(produit);

            }
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                Connexion.close(rs);
                Connexion.close(stm);
                Connexion.close(cnx);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }

        }
        return produits;

    }

    public List<Produit> listerProduitsSuggeresITEM() {
        Connection cnx = null;
        PreparedStatement stm = null;
        ResultSet rs = null;

        List<Produit> produits = new ArrayList<>();

        try {

            cnx = Connexion.getConnexion();
            stm = cnx.prepareStatement(SQL_SELECT_BY_SUGGERE);

            rs = stm.executeQuery();

            while (rs.next()) {

                Produit produit = new Produit();
                int idProduit = rs.getInt("id_produit");
                String nomProduit = rs.getString("nom_produit");
                double prix = rs.getDouble("prix");
                String description = rs.getString("description_produit");
                int prod_demande = rs.getInt("prod_demande");
                String image = rs.getString("image");
                int idAnimal = rs.getInt("id_animal");
                int prod_suggere = rs.getInt("prod_suggere");

                produit = new Produit(idProduit, nomProduit, prix, description, prod_demande, image, idAnimal, prod_suggere);

                produits.add(produit);

            }
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                Connexion.close(rs);
                Connexion.close(stm);
                Connexion.close(cnx);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }

        }
        return produits;

    }

    public List<Produit> listerProduitsByCategorie(int idAnimal) {
        Connection cnx = null;
        PreparedStatement stm = null;
        ResultSet rs = null;
        Produit produit;
        List<Produit> produits = new ArrayList<>();

        try {

            cnx = Connexion.getConnexion();
            stm = cnx.prepareStatement(SQL_SELECT_BY_ANIMAL);
            stm.setInt(1, idAnimal);
            rs = stm.executeQuery();

            while (rs.next()) {
                produit = new Produit();
                produit.setIdProduit(rs.getInt("id_produit"));
                produit.setNomProduit(rs.getString("nom_produit"));
                produit.setPrix(rs.getDouble("prix"));
                produit.setDescription(rs.getString("description_produit"));
                produit.setProduitDemande(rs.getInt("prod_demande"));
                produit.setImage(rs.getString("image"));
                produit.setIdAnimal(rs.getInt("id_animal"));
                produit.setProduitSuggere(rs.getInt("prod_suggere"));

                produits.add(produit);

            }
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                Connexion.close(rs);
                Connexion.close(stm);
                Connexion.close(cnx);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }

        }
        return produits;

    }

    public static Produit consulterProduct(int idProduit) {

        Connection cnx = null;
        PreparedStatement stm = null;
        ResultSet rs = null;
        Produit produit = null;
        try {

           
            cnx = Connexion.getConnexion();
            stm = cnx.prepareStatement(SQL_SELECT_PRODUIT_BY_ID);
            stm.setInt(1, idProduit);
            rs = stm.executeQuery();

            if (rs.next()) {
                produit = new Produit();
                produit.setIdProduit(rs.getInt("id_produit"));
                produit.setNomProduit(rs.getString("nom_produit"));
                produit.setPrix(rs.getDouble("prix"));
                produit.setDescription(rs.getString("description_produit"));
                produit.setProduitDemande(rs.getInt("prod_demande"));
                produit.setImage(rs.getString("image"));
                produit.setIdAnimal(rs.getInt("id_animal"));
                produit.setProduitSuggere(rs.getInt("prod_suggere"));

            }
            
        } catch (SQLException ex) {
            return null;
        } finally {
            try {

                Connexion.close(rs);
                Connexion.close(stm);
                Connexion.close(cnx);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }

        }
        return produit;
    }

}
