package com.model.dao;

import com.model.entities.Animal;
import com.service.Connexion;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Nadia Garcia
 */
public class AnimalDAO {

    private static final String SQL_SELECT_ALL = "SELECT id_animal, nom_animal FROM animal";
    private static final String SQL_SELECT_ANIMAL = "SELECT id_animal, nom_animal FROM animal WHERE id_animal = ?";
    private static final String SQL_INSERT_ANIMAL = "INSERT INTO animal (nom_animal) VALUES (?)";
    private static final String SQL_UPDATE_ANIMAL = "UPDATE animal SET nom_animal = ? WHERE id_animal = ?";
    private static final String SQL_DELETE_ANIMAL = "DELETE FROM animal WHERE id_animal = ?";

    public List<Animal> selectionnerALL() throws ClassNotFoundException {

        Connection cnx = null;
        PreparedStatement stm = null;
        ResultSet rs = null;
        Animal a = null;

        List<Animal> animals = new ArrayList<>();

        try {

            cnx = Connexion.getConnexion();
            stm = cnx.prepareStatement(SQL_SELECT_ALL);
            rs = stm.executeQuery();

            while (rs.next()) {
                int idAnimal = rs.getInt("id_animal");
                String nomAnimal = rs.getString("nom_animal");

                a = new Animal(idAnimal, nomAnimal);

                animals.add(a);

            }
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                Connexion.close(rs);
                Connexion.close(stm);
                Connexion.close(cnx);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }

        }
        return animals;
    }

    public Animal trouverAnimal(Animal animal) {
        Connection cnx = null;
        PreparedStatement stm = null;
        ResultSet rs = null;

        try {

            cnx = Connexion.getConnexion();
            stm = cnx.prepareStatement(SQL_SELECT_ANIMAL);
            stm.setInt(1, animal.getIdAnimal());
            rs = stm.executeQuery();

            while (rs.next()) {
                animal.setIdAnimal(rs.getInt("id_animal"));
                animal.setNomAnimal(rs.getString("nom_animal"));
             
            }

        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {

                Connexion.close(rs);
                Connexion.close(stm);
                Connexion.close(cnx);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }

        }

        return animal;

    }

    public int creerAnimal(Animal animal) throws SQLException {
        Connection cnx = null;
        PreparedStatement stm = null;
        int rows = 0;
        try {
            cnx = Connexion.getConnexion();
            stm = cnx.prepareStatement(SQL_INSERT_ANIMAL);
            stm.setString(1, animal.getNomAnimal());

            rows = stm.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            Connexion.close(stm);
            Connexion.close(cnx);
        }
        return rows;

    }

    public int modifierAnimal(Animal animal) throws SQLException {
        Connection cnx = null;
        PreparedStatement stm = null;
        int rows = 0;
        try {
            cnx = Connexion.getConnexion();
            stm = cnx.prepareStatement(SQL_UPDATE_ANIMAL);
            stm.setString(1, animal.getNomAnimal());
            stm.setInt(2, animal.getIdAnimal());

            rows = stm.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            Connexion.close(stm);
            Connexion.close(cnx);
        }
        return rows;
    }
    
     public int eliminerAnimal(Animal animal) throws SQLException {
        Connection cnx = null;
        PreparedStatement stm = null;
        int rows = 0;
        try {
            cnx = Connexion.getConnexion();
            stm = cnx.prepareStatement(SQL_DELETE_ANIMAL);
            stm.setInt(1, animal.getIdAnimal());

            rows = stm.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            Connexion.close(stm);
            Connexion.close(cnx);
        }
        return rows;
    }

}
