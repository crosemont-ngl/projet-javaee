/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.model.dao;

import com.model.entities.Utilisateur;
import com.service.Connexion;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author UtilisateurDAO
 */
public class UtilisateurDAO {

    private static final String SQL_SELECT_BY_EMAIL_MOTPASSE = "SELECT id_utilisateur, nom_utilisateur, email, mot_passe, type_utilisateur FROM utilisateur WHERE email = ? AND mot_passe = ? ";
    private static final String SQL_SELECT_BY_EMAIL = "SELECT id_utilisateur, nom_utilisateur, email, mot_passe, type_utilisateur FROM utilisateur WHERE email = ?";
    private static final String SQL_INSERT_UTILISATEUR = "INSERT INTO utilisateur (nom_utilisateur, email, mot_passe, type_utilisateur) VALUES (?,?,?,?)";

//    public int trouverUtilisateur(Utilisateur utilisateur) throws SQLException {
//        Connection cnx = null;
//        PreparedStatement stm = null;
//        ResultSet rs = null;
//        int r = 0;
//
//        try {
//            cnx = Connexion.getConnexion();
//            stm = cnx.prepareStatement(SQL_SELECT_BY_EMAIL_MOTPASSE);
//            stm.setString(1, utilisateur.getEmail());
//            stm.setString(2, utilisateur.getMotPasse());
//            rs = stm.executeQuery();
//
//            while (rs.next()) {
//                utilisateur.setEmail(rs.getString("email"));
//                utilisateur.setMotPasse(rs.getString("mot_passe"));
//                
//                r= rs.getRow();
//            }
//        } catch (SQLException ex) {
//            ex.printStackTrace(System.out);
//        } finally {
//            Connexion.close(rs);
//            Connexion.close(stm);
//            Connexion.close(cnx);
//        }
//        return r;
//    }
    public Utilisateur trouverUtilisateur(Utilisateur utilisateur) throws SQLException {
        Connection cnx = null;
        PreparedStatement stm = null;
        ResultSet rs = null;
        int r = 0;

        try {
            cnx = Connexion.getConnexion();
            stm = cnx.prepareStatement(SQL_SELECT_BY_EMAIL_MOTPASSE);
            stm.setString(1, utilisateur.getEmail());
            stm.setString(2, utilisateur.getMotPasse());
            rs = stm.executeQuery();

            while (rs.next()) {
                utilisateur.setIdUtilisateur(rs.getInt("id_utilisateur"));
                utilisateur.setNomUtilisateur(rs.getString("nom_utilisateur"));
                utilisateur.setEmail(rs.getString("email"));
                utilisateur.setMotPasse(rs.getString("mot_passe"));
                utilisateur.setTypeUtilisateur(rs.getString("type_utilisateur"));

                r = rs.getRow();
            }
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            Connexion.close(rs);
            Connexion.close(stm);
            Connexion.close(cnx);
        }
        return utilisateur;
    }

    public Utilisateur selectionnerByEmail(Utilisateur utilisateur) {
        Connection cnx = null;
        PreparedStatement stm = null;
        ResultSet rs = null;
        int r = 0;
        try {
            cnx = Connexion.getConnexion();
            stm = cnx.prepareStatement(SQL_SELECT_BY_EMAIL);
            stm.setString(1, utilisateur.getEmail());
            rs = stm.executeQuery();

            while (rs.next()) {
                utilisateur.setIdUtilisateur(rs.getInt("id_utilisateur"));
                utilisateur.setNomUtilisateur(rs.getString("id_utilisateur"));
                utilisateur.setEmail(rs.getString("email"));
                utilisateur.setMotPasse("mot_passe");
                utilisateur.setTypeUtilisateur("type_utilisateur");
                r = rs.getRow();
            }
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                Connexion.close(rs);
                Connexion.close(stm);
                Connexion.close(cnx);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return utilisateur;

    }

    public int creerUtilisateur(Utilisateur utilisateur) {
        Connection cnx = null;
        PreparedStatement stm = null;

        int enregistrementCree = 0;

        try {
            cnx = Connexion.getConnexion();
            stm = cnx.prepareStatement(SQL_INSERT_UTILISATEUR);
            stm.setString(1, utilisateur.getNomUtilisateur());
            stm.setString(2, utilisateur.getEmail());
            stm.setString(3, utilisateur.getMotPasse());
            stm.setString(4, utilisateur.getTypeUtilisateur());
            enregistrementCree = stm.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                Connexion.close(stm);
                Connexion.close(cnx);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }

        return enregistrementCree;
    }

}
