# Projet JavaEE 

Projet final cours 420-AV4-RO CONCEPTION D'APPLICATIONS HYPERMÉDIAS II 

E-Kusa est un site web d’e-commerce orienté vers la vente de produits et des services pour l'alimentation des animaux. 

Caractéristiques techniques du site :
	* Créateur du site web : Nadia Garcia
	* Langages utilisés : JavaEE – JavaScript – HTML – CSS - Bootstrap
	* Base de données : MySQL
	* Serveur : Tomcat
	* IDE : NetBeans

Rôles :  visiteur - client – administrateur 
	* Étant que visiteur, on peut juste parcourir le site web. 
	* Étant que client, on peut magasiner sur le site et ajouter des produits dans son panier d’achat. 
	* Pour devenir client, la personne doit s’inscrire avant. 
	* Étant qu’administrateur on a l’accès au couche de backend du site web. 