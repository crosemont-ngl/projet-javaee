CREATE DATABASE  IF NOT EXISTS `eboutique` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `eboutique`;
-- MySQL dump 10.13  Distrib 8.0.18, for Win64 (x86_64)
--
-- Host: localhost    Database: eboutique
-- ------------------------------------------------------
-- Server version	8.0.18

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `achat`
--

DROP TABLE IF EXISTS `achat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `achat` (
  `id_achat` int(11) NOT NULL AUTO_INCREMENT,
  `id_utilisateur` int(11) NOT NULL,
  `date_achat` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `montant` double NOT NULL,
  PRIMARY KEY (`id_achat`),
  KEY `compras_ibfk_2_idx` (`id_utilisateur`),
  CONSTRAINT `achat_ibfk_2` FOREIGN KEY (`id_utilisateur`) REFERENCES `utilisateur` (`id_utilisateur`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `achat`
--

LOCK TABLES `achat` WRITE;
/*!40000 ALTER TABLE `achat` DISABLE KEYS */;
INSERT INTO `achat` (id_achat, id_utilisateur, date_achat, montant) VALUES (1,2,'2020-12-09 01:40:13',10.23);
/*!40000 ALTER TABLE `achat` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `animal`
--

DROP TABLE IF EXISTS `animal`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `animal` (
  `id_animal` int(11) NOT NULL AUTO_INCREMENT,
  `nom_animal` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_animal`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `animal`
--

LOCK TABLES `animal` WRITE;
/*!40000 ALTER TABLE `animal` DISABLE KEYS */;
INSERT INTO `animal` (id_animal, nom_animal) VALUES (1,'chat'),(2,'chien'),(3,'reptile'),(4,'oiseau domestique'),(5,'petit animal');
/*!40000 ALTER TABLE `animal` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `detaille_achat`
--

DROP TABLE IF EXISTS `detaille_achat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `detaille_achat` (
  `id_detaille` int(11) NOT NULL AUTO_INCREMENT,
  `id_achat` int(11) NOT NULL,
  `id_produit` int(11) NOT NULL,
  `quantite` int(11) NOT NULL,
  PRIMARY KEY (`id_detaille`),
  KEY `detaille_fk_achat_idx` (`id_achat`),
  KEY `detaille_fk_prod_idx` (`id_produit`),
  CONSTRAINT `detaille_fk_achat` FOREIGN KEY (`id_achat`) REFERENCES `achat` (`id_achat`),
  CONSTRAINT `detaille_fk_prod` FOREIGN KEY (`id_produit`) REFERENCES `produit` (`id_produit`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `detaille_achat`
--

LOCK TABLES `detaille_achat` WRITE;
/*!40000 ALTER TABLE `detaille_achat` DISABLE KEYS */;
/*!40000 ALTER TABLE `detaille_achat` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `produit`
--

DROP TABLE IF EXISTS `produit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `produit` (
  `id_produit` int(11) NOT NULL AUTO_INCREMENT,
  `nom_produit` varchar(255) NOT NULL,
  `prix` decimal(10,2) NOT NULL,
  `description_produit` varchar(50) NOT NULL,
  `prod_demande` int(1) NOT NULL DEFAULT '1',
  `image` varchar(255) NOT NULL DEFAULT 'demo.png',
  `id_animal` int(11) NOT NULL,
  `prod_suggere` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_produit`),
  KEY `FK_ProduitAnimal` (`id_animal`),
  CONSTRAINT `FK_ProduitAnimal` FOREIGN KEY (`id_animal`) REFERENCES `animal` (`id_animal`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `produit`
--

LOCK TABLES `produit` WRITE;
/*!40000 ALTER TABLE `produit` DISABLE KEYS */;
INSERT INTO `produit` (id_produit, nom_produit, prix, description_produit, prod_demande, image, id_animal, prod_suggere) VALUES (2,'Vetdiet',20.59,'Nourriture  au poulet',1,'product2.jpg',1,0),(3,'Vetdiet',2.28,'Nourriture humide sans grains ',1,'product3.jpg',2,0),(4,'Hagen',10.49,'Mélange Gourmet pour perruches',1,'product4.jpg',4,0),(5,'Prestige Premium',12.49,'Mélange de graines pour perruches',1,'product5.jpg',4,0),(6,'Exo Terra',10.29,'Aliment pour geckos à crête',1,'product6.jpg',3,0),(7,'Vetdiet',25.59,'Nourriture sèche au saumon ',0,'product11.jpg',2,0),(8,'Pro Plan',68.39,'Nourriture sèche bouchées de bœuf et riz ',0,'product12.jpg',2,0),(9,'Big Country Raw',9.49,'Cous de dinde',0,'product9.jpg',1,0),(10,'PureBites',9.49,'Recette lyophilisée pour chats',0,'product10.jpg',1,0),(11,'CaniSource',23.49,'Nourriture crue déshydratée au poisson',0,'product7.jpg',1,1),(12,'CaniSource',23.49,'Nourriture  crue déshydratée',0,'product8.jpg',1,1),(13,'Blue',26.79,'Formule Santé Digestive',0,'product13.jpg',2,1),(14,'Nutrience Care',28.29,'Formule peau et estomac sensibles',0,'product14.jpg',2,2),(15,'Royal Canin',3.79,'Nourriture humide  pour chiens',0,'product15.jpg',2,2),(16,'Oxbow',28.19,'Nourriture pour cochons d\'Inde adultes',0,'product16.jpg',5,2),(17,'Hagen',6.49,'Mélange de base EVM',0,'product20.jpg',5,0),(18,'Nature',14.59,'Nourriture Fiberfood ',0,'product17.jpg',5,0),(19,'Nature',7.29,'Nourriture pour mini hamster',0,'product18.jpg',5,0),(20,'Nature',9.89,'Nourriture pour hamster',0,'product19.jpg',5,0),(21,'Living World',7.39,'Aliment Extrusion pour cochons d\'Inde',0,'product21.jpg',5,0),(22,'Exo Terra',6.49,'Granulés  à base d’insectes',0,'product22.jpg',3,0),(23,'Nutrafin',5.19,'Microgranulés Bug Bites ',0,'product23.jpg',3,0),(24,'Exo Terra',10.19,'Aliment en coupe ',0,'product24.jpg',3,0),(25,'Nutrafin',9.59,'Bâtonnets Bug Bites',0,'product25.jpg',3,0),(26,'Tropican',8.29,'Aliment Hand-Feeding ',0,'product26.jpg',4,0),(27,'Tropimix',104.19,'Aliment pour grands perroquets',0,'product27.jpg',4,0),(28,'Orlux',30.39,'Aliment complet ',0,'product28.jpg',4,0),(29,'Goldenfeast',24.29,'Mélange des Caraïbes',0,'product29.jpg',4,0),(39,'Vetdiet',24.60,'Nourriture  soins dentaires',1,'product1.jpg',1,0);
/*!40000 ALTER TABLE `produit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `utilisateur`
--

DROP TABLE IF EXISTS `utilisateur`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `utilisateur` (
  `id_utilisateur` int(11) NOT NULL AUTO_INCREMENT,
  `nom_utilisateur` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `mot_passe` varchar(255) NOT NULL,
  `type_utilisateur` enum('A','NA') NOT NULL DEFAULT 'NA',
  PRIMARY KEY (`id_utilisateur`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `utilisateur`
--

LOCK TABLES `utilisateur` WRITE;
/*!40000 ALTER TABLE `utilisateur` DISABLE KEYS */;
INSERT INTO `utilisateur` (id_utilisateur, nom_utilisateur, email, mot_passe, type_utilisateur) VALUES (1,'Nadia Garcia','nadia@email.com','NaGa11ene','A'),(2,'Jhon Linch','userA@email.com','NaGa11ene','NA'),(3,'Frank K','userB@email.com','NaGa11ene','NA'),(4,'Charles B','userC@email.com','NaGa11ene','NA'),(5,'Tomas Kim','userD@correo.com','NaGa11ene','NA'),(6,'Evelyn Garcia','userE@correo.com','NaGa11ene','NA'),(7,'George  B','userF@correo.com','NaGa11ene','NA'),(8,'Nadia','userH@email.com','NaGa11ene','NA');
/*!40000 ALTER TABLE `utilisateur` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-12-09  4:15:21
